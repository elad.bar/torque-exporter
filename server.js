const express = require("express");

const { APIMiddleware } = require("./common/middlewares.js");
const { PrometheusManager } = require("./managers/prometheusManager.js");
const { TorqueManager } = require("./managers/torqueManager.js");

const {
  CONFIG_FILE,
  API_PORT,
  ENDPOINT_TORQUE_METRICS,
  ENDPOINT_TORQUE,
  TORQUE_STATS_RESPONSE_CONTENT,
  ENDPOINT_TORQUE_PING
} = require("./common/consts.js");

class TorqueExporter {
  constructor() {
    this.api = express();
    this.api.use(express.json());
    this.api.use(
      express.urlencoded({
        extended: true,
      })
    );

    this.config = require(CONFIG_FILE);
    this.middleware = new APIMiddleware(this.config);
    this.prometheusManager = new PrometheusManager();
    this.torqueManager = new TorqueManager(this.prometheusManager, this.config);
  }

  initialize() {
    console.debug("Initializing TorqueAPI");

    this.middleware.Initialize();

    if (this.middleware.enabled) {
      console.info(`Starting API on port ${API_PORT}`);

      this.bindEndpoints();

      this.api.listen(API_PORT);
    } else {
      console.error(`Cannot start API`);
    }
  }

  bindEndpoints() {
    const apiTokenCheck = this.middleware.apiTokenCheck.bind(this.middleware);
    const userCheck = this.middleware.userCheck.bind(this.middleware);

    this.api.get(ENDPOINT_TORQUE_METRICS, apiTokenCheck, async (req, res) => {
      const content = await this.prometheusManager.getMetrics();
      const contentType = this.prometheusManager.getMetricsContentType();

      res.set("Content-Type", contentType);

      this.setResponseStatus(res, 200, content);
    });

    this.api.post(ENDPOINT_TORQUE, [apiTokenCheck], (req, res) => {
        this.processData(res, req.body);
    });

    this.api.get(ENDPOINT_TORQUE, [apiTokenCheck, userCheck], (req, res) => {
      this.processData(res, req.query);
    });

    this.api.get(ENDPOINT_TORQUE_PING, (req, res) => {
      res.status(200).send(TORQUE_STATS_RESPONSE_CONTENT);
    });

    ENDPOINT_TORQUE_PING
  }

  processData(res, data) {
    if (!data) {
      this.setResponseStatus(res, 400, "Bad Request");
    } else {
      try {
        this.torqueManager.process(data);

        this.setResponseStatus(res, 200, TORQUE_STATS_RESPONSE_CONTENT);
      } catch (ex) {
        this.setResponseStatus(res, 400, ex.toString());
      }
    }
  }

  setResponseStatus(response, statusCode, content) {
    response.status(statusCode);

    if (content) {
      response.send(content);

      if (statusCode >= 400) {
        console.error(content);
      }
    }
  }
}

const torqueExporter = new TorqueExporter();
torqueExporter.initialize();
