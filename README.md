# Torque Exporter
Prometheus exporter for Torque mobile app,
After 1 minute of inactivity, will send reset metrics request to zero all sensors.

[GitLab](https://gitlab.com/elad.bar/torque-exporter)

## How to use

### Run docker container using the following Docker Compose
```yaml
  torque-exporter:
    image: registry.gitlab.com/elad.bar/torque-exporter:latest
    restart: unless-stopped
    hostname: torque-exporter
    container_name: torque-exporter
    ports:
      - 8128:8128
    volumes:
      - ./config/config.json:/usr/src/app/config.json
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro

```

### Setup Tourqe App

In *Settings -> Data Logging & Upload*

Under the *Logging Preferences* header:

Touch *Select what to log*, activate the menu in the upper right, and select *Add PID to log*.
Select items of interest.

Under the *Realtime Web Upload* header:

Check *Upload to web-server*.
- Enter https://HOST:PORT/api/torque as the *Web-server URL*, where HOST and PORT are your externally accessible Torque API HTTP host and port. 
- Enter an email address in User Email Address (Same email address as in the `devices.yaml`)

### Configuration

Configuration file should be available at `/usr/src/app/config.json`, example:

```json
{
    "apiToken": "Bearer {token}", 
    "devices": {
        "email@email.com": "Name of the Car"
    }
}
```

#### Authentication Section
Represent configurations related to access the API, avoid configuring them will fail the load of the API 

##### Devices
Key-value pairs of email as configured in the TorqueApp and the desired device name in the output plugins

```json
{
    "authentication": {
        "devices": {
            "email@email.com": "Name of the Car"
        }
    }
}
```

## Endpoints
API Token must be sent within the header 
```yaml
Authorization: Bearer {token}
```


Endpint | Method | Description | 
---|---|---|
/api/torque | GET | Report statistics (For Torque App) |
/api/torque | POST | Report statistics (For testing) |
/metrics | GET | Get metrics for prometheus |
/ping | GET | Returns Ok! (HTTP Code 200) message for healthcheck, works without API Token |

### Prometheus metrics output (/metrics)

```
# HELP torque_obd_sensor Torque OBDII Sensor
# TYPE torque_obd_sensor gauge
torque_obd_sensor{version="9", session="123", id="abc", unit="°", name="GPSLon", description="GPS Longitude", device="Car"} 30.91186564252267
torque_obd_sensor{version="9", session="123", id="abc", unit="°", name="GPSLat", description="GPS Latitude", device="Car"} 30.24246221282244
torque_obd_sensor{version="9", session="123", id="abc", unit="km/h", name="GPS Spd", description="Speed (GPS)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="A THR2", description="Absolute Throttle Position B", device="Car"} 51.372549019607845
torque_obd_sensor{version="9", session="123", id="abc", unit="g", name="Accel", description="Acceleration Sensor(Total)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="g", name="Accel(x)", description="Acceleration Sensor(X axis)", device="Car"} 0.1
torque_obd_sensor{version="9", session="123", id="abc", unit="g", name="Accel(y)", description="Acceleration Sensor(Y axis)", device="Car"} 0.5
torque_obd_sensor{version="9", session="123", id="abc", unit="g", name="Accel(z)", description="Acceleration Sensor(Z axis)", device="Car"} 0.75
torque_obd_sensor{version="9", session="123", id="abc", unit=":1", name="AFR(c)", description="Air Fuel Ratio(Commanded)", device="Car"} 14.708523559570311
torque_obd_sensor{version="9", session="123", id="abc", unit=":1", name="AFR(m)", description="Air Fuel Ratio(Measured)", device="Car"} 14.7605787
torque_obd_sensor{version="9", session="123", id="abc", unit="°C", name="Air temp", description="Ambient air temp", device="Car"} 32
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="BATLVL", description="Android device Battery Level", device="Car"} 67
torque_obd_sensor{version="9", session="123", id="abc", unit="km/h", name="Trip Speed", description="Average trip speed(whilst stopped or moving)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="mb", name="Baro", description="Barometer (on Android device)", device="Car"} 1016.59228515625
torque_obd_sensor{version="9", session="123", id="abc", unit="psi", name="Baro", description="Barometric pressure (from vehicle)", device="Car"} 101
torque_obd_sensor{version="9", session="123", id="abc", unit="°C", name="Cat B1S1", description="Catalyst Temperature (Bank 1,Sensor 1)", device="Car"} 517.8
torque_obd_sensor{version="9", session="123", id="abc", unit="°C", name="Cat B1S2", description="Catalyst Temperature (Bank 1,Sensor 2)", device="Car"} 387.3
torque_obd_sensor{version="9", session="123", id="abc", unit="", name="COMEQR", description="Commanded Equivalence Ratio(lambda)", device="Car"} 1.000579833984375
torque_obd_sensor{version="9", session="123", id="abc", unit="km", name="Dist Empt.", description="Distance to empty (Estimated)", device="Car"} 309.213766046163
torque_obd_sensor{version="9", session="123", id="abc", unit="km", name="MIL Off", description="Distance travelled since codes cleared", device="Car"} 2786
torque_obd_sensor{version="9", session="123", id="abc", unit="km", name="MIL On", description="Distance travelled with MIL/CEL lit", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="psi", name="DPFB1DP", description="DPF Bank 1 Delta Pressure", device="Car"} -0.11
torque_obd_sensor{version="9", session="123", id="abc", unit="psi", name="DPFB1OP", description="DPF Bank 1 Outlet Pressure", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="°C", name="Coolant", description="ECU(7EA): Engine Coolant Temperature", device="Car"} 72
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="Load", description="ECU(7EA): Engine Load", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="Abs Load", description="Engine Load(Absolute)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="°C", name="Oil Temp", description="Engine Oil Temperature", device="Car"} 73
torque_obd_sensor{version="9", session="123", id="abc", unit="rpm", name="Revs", description="ECU(7EA): Engine RPM", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="cost", name="Fuel Cost", description="Fuel cost (trip)", device="Car"} 0.000011432604226820243
torque_obd_sensor{version="9", session="123", id="abc", unit="l/hr", name="Fuel Flow", description="Fuel flow rate/hour", device="Car"} 0.0036396489574547666
torque_obd_sensor{version="9", session="123", id="abc", unit="cc/min", name="Fuel Flow", description="Fuel flow rate/minute", device="Car"} 0.060660815957579446
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="Fuel Rem", description="Fuel Remaining (Calculated from vehicle profile)", device="Car"} 61.21239089949723
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="LTFT1", description="Fuel Trim Bank 1 Long Term", device="Car"} 0.78125
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="STFT1", description="Fuel Trim Bank 1 Short Term", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="l", name="Fuel Used", description="Fuel used (trip)", device="Car"} 0.000031757232701470514
torque_obd_sensor{version="9", session="123", id="abc", unit="m", name="GPS Acc", description="GPS Accuracy", device="Car"} 4
torque_obd_sensor{version="9", session="123", id="abc", unit="m", name="GPS Height", description="GPS Altitude", device="Car"} 65.04238777175628
torque_obd_sensor{version="9", session="123", id="abc", unit="°", name="GPS Brng", description="GPS Bearing", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="", name="GPS Sat", description="GPS Satellites", device="Car"} 28
torque_obd_sensor{version="9", session="123", id="abc", unit="km/h", name="Spd Diff", description="GPS vs OBD Speed difference", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="°C", name="Intake", description="ECU(7EA): Intake Air Temperature", device="Car"} 32
torque_obd_sensor{version="9", session="123", id="abc", unit="psi", name="MAP-A", description="ECU(7EA): Intake Manfold Abs Pressure A", device="Car"} 101.09375
torque_obd_sensor{version="9", session="123", id="abc", unit="psi", name="MAP-B", description="Intake Manfold Abs Pressure B", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="kpl", name="KPL", description="Kilometers Per Litre(Instant)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="kpl", name="KPL(avg)", description="Kilometers Per Litre(Long Term Average)", device="Car"} 9.184352389533254
torque_obd_sensor{version="9", session="123", id="abc", unit="l/100km", name="LPK(avg)", description="Litres Per 100 Kilometer(Long Term Average)", device="Car"} 10.887857494880143
torque_obd_sensor{version="9", session="123", id="abc", unit="g/s", name="MAF", description="Mass Air Flow Rate", device="Car"} 0.01
torque_obd_sensor{version="9", session="123", id="abc", unit="mpg", name="MPG", description="Miles Per Gallon(Instant)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="mpg", name="MPG(avg)", description="Miles Per Gallon(Long Term Average)", device="Car"} 25.94449827551767
torque_obd_sensor{version="9", session="123", id="abc", unit="mA", name="O2S1C", description="O2 Sensor1 Wide Range Current", device="Car"} 0.0078125
torque_obd_sensor{version="9", session="123", id="abc", unit="λ", name="O2B1S1Eq", description="O2 Bank 1 Sensor 1 Wide Range Equivalence Ratio", device="Car"} 1.004121
torque_obd_sensor{version="9", session="123", id="abc", unit="mA", name="O2B1S2C", description="O2 Bank 1 Sensor 2 Wide Range Current", device="Car"} -0.0078125
torque_obd_sensor{version="9", session="123", id="abc", unit="λ", name="O2B1S2Eq", description="O2 Bank 1 Sensor 2 Wide Range Equivalence Ratio", device="Car"} 0.999424
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="R THR", description="Relative Throttle Position", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="s", name="RunTime", description="ECU(7EA): Run time since engine start", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="km/h", name="Speed", description="ECU(7EA): Speed (OBD)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="%", name="Throttle", description="ECU(7EA): Throttle Position(Manifold)", device="Car"} 19.607843137254903
torque_obd_sensor{version="9", session="123", id="abc", unit="°", name="Timing Adv", description="Timing Advance", device="Car"} 5
torque_obd_sensor{version="9", session="123", id="abc", unit="km", name="Odo", description="Trip distance (stored in vehicle profile)", device="Car"} 237.62887542481243
torque_obd_sensor{version="9", session="123", id="abc", unit="s", name="TripTime", description="Trip Time(Since journey start)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="s", name="Moving", description="Trip time(whilst moving)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="s", name="Stopped", description="Trip time(whilst stationary)", device="Car"} 0
torque_obd_sensor{version="9", session="123", id="abc", unit="psi", name="Boost", description="Turbo Boost & Vacuum Gauge", device="Car"} 0.013597287918456047
torque_obd_sensor{version="9", session="123", id="abc", unit="V", name="Volts(CM)", description="ECU(7EA): Voltage (Control Module)", device="Car"} 12.651
torque_obd_sensor{version="9", session="123", id="abc", unit="V", name="Volts(Ad)", description="Voltage (OBD Adapter)", device="Car"} 12.600000381469727

# HELP torque_device_location Torque Device Location
# TYPE torque_device_location gauge
torque_device_location{version="9", session="123", id="abc", device="Car", geohash="geo_hash"} 1
```

