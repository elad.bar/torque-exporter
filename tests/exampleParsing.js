const {TorqueManager} = require("./managers/torqueManager.js");


class TorqueTest {
    constructor() {
        this.jsonDefaults = require("../examples/defaults.json");
        this.jsonUser = require("../examples/user.json");
        this.jsonValues = require("../examples/values.json");

        this.torqueManager = new TorqueManager();
    };

    Initialize() {
        console.debug("Initializing Torque Test");  

        this.torqueManager.process(this.jsonDefaults);
        this.torqueManager.process(this.jsonUser);
        this.torqueManager.process(this.jsonValues);
    };
};

module.exports.TorqueTest = TorqueTest;