const {
  MESSAGE_TYPE_DEFAULT_METADATA,
  MESSAGE_TYPE_USER_METADATA,
  MESSAGE_TYPE_DATA,
  METADATA_FIELDS,
  PID_FIELDS,
  TORQUE_METRIC_LABEL_SESSION,
  TORQUE_METRIC_LABEL_ID,
  MESSAGE_TYPE,
  PID_DEFAULT_UNIT,
  PID_METADATA,
  PID_MAPPING,
  PID_UNIT,
  METRIC_VALUE_KEY,
  TORQUE_METRIC_LABEL_CLEAN,
  TORQUE_METRIC_LABEL_DEVICE,
  TORQUE_IGNORE_PIDS,
  PID_GPS_LATITUDE,
  PID_GPS_LONGITUDE,
  PID_GPS_LONGITUDE_NAME,
  PID_GPS_LATITUDE_NAME
} = require("../common/consts.js");

class TorqueManager {
  constructor(prometheusClient, config) {
    this.prometheusClient = prometheusClient;
    this.config = config;

    this.sessionConfiguration = {};
    this.autoResetId = null;

    this.messageParsers = {
      [MESSAGE_TYPE_DEFAULT_METADATA]: (metadata, data) =>
        this.parseDefaultMetadata(metadata, data),
      [MESSAGE_TYPE_USER_METADATA]: (metadata, data) =>
        this.parseUserMetadata(metadata, data),
      [MESSAGE_TYPE_DATA]: (metadata, data) => this.parseData(metadata, data),
    };

    this.metadataKeys = Object.keys(METADATA_FIELDS);
  }

  isSessionMetadataReady(metadata) {
    const sessionConfiguration = this.getSessionConfiguration(metadata);

    const sessionConfigurationKeys = !!sessionConfiguration
      ? Object.keys(sessionConfiguration)
      : [];

    const availableFields = PID_FIELDS.filter((f) =>
      sessionConfigurationKeys.includes(f)
    ).length;

    return availableFields === PID_FIELDS.length;
  }

  getSessionKey(metadata) {
    const session = metadata[TORQUE_METRIC_LABEL_SESSION];
    const id = metadata[TORQUE_METRIC_LABEL_ID];

    const key = `${id}_${session}`;

    return key;
  }

  getSessionConfiguration(metadata) {
    const sessionKey = this.getSessionKey(metadata);
    const sessionConfiguration = this.sessionConfiguration[sessionKey];

    return sessionConfiguration;
  }

  getMessageType(dataKeys) {
    const messageTypeKeys = Object.keys(MESSAGE_TYPE);
    const firstDataKey = dataKeys[0];

    const messageTypeKeysFound = messageTypeKeys.filter((k) =>
      firstDataKey.startsWith(k)
    );
    const messageTypeKey = messageTypeKeysFound[0];

    const messageType = MESSAGE_TYPE[messageTypeKey];

    return messageType;
  }

  extractMetadata(data) {
    const result = {};
    const dataKeys = Object.keys(data);

    dataKeys
      .filter((k) => this.metadataKeys.includes(k))
      .forEach((k) => {
        const keyName = METADATA_FIELDS[k];
        result[keyName] = data[k];
      });

    return result;
  }

  extractData(data) {
    const result = {};
    const dataKeys = Object.keys(data);

    dataKeys
      .filter((k) => !this.metadataKeys.includes(k))
      .forEach((k) => {
        result[k] = data[k];
      });

    return result;
  }

  updateSessionConfiguration(metadata, key, values) {
    const sessionKey = this.getSessionKey(metadata);
    const sessionConfiguration = this.getSessionConfiguration(metadata);

    if (!!sessionConfiguration) {
      const updatedConfiguration = {
        ...sessionConfiguration,
        [key]: values,
      };

      this.sessionConfiguration[sessionKey] = updatedConfiguration;
    } else {
      this.sessionConfiguration[sessionKey] = {
        [key]: values,
      };
    }
  }

  parseDefaultMetadata(metadata, data) {
    try {
      const dataKeys = Object.keys(data);
      const defaultMetadata = {};

      dataKeys.forEach((k) => {
        const cleanKey = k.replace(PID_DEFAULT_UNIT, "");
        defaultMetadata[cleanKey] = data[k];
      });

      this.updateSessionConfiguration(
        metadata,
        PID_DEFAULT_UNIT,
        defaultMetadata
      );
    } catch (ex) {
      console.error(
        `Failed to parse default metadata from request, Error: ${ex}`
      );
      throw Error("Failed to parse default metadata");
    }
  }

  parseUserMetadata(metadata, data) {
    try {
      const dataKeys = Object.keys(data);
      const defaultMetadata = {};
      const mappingFieldKeys = Object.keys(PID_METADATA);

      dataKeys.forEach((k) => {
        const defaultMetadataKeys = Object.keys(defaultMetadata);
        const prefix = mappingFieldKeys.filter((f) => k.startsWith(f))[0];
        const pid = k.replace(prefix, "");
        const fieldItem = PID_METADATA[prefix];

        if (!defaultMetadataKeys.includes(pid)) {
          defaultMetadata[pid] = {};
        }

        const currentPIDConfiguration = defaultMetadata[pid];
        const value = Array.isArray(data[k]) ? data[k][0] : data[k];

        currentPIDConfiguration[fieldItem] = value;

        defaultMetadata[pid] = currentPIDConfiguration;
      });

      this.updateSessionConfiguration(
        metadata,
        PID_MAPPING,
        defaultMetadata
      );
    } catch (ex) {
      console.error(`Failed to parse metadata from request, Error: ${ex}`);
      throw Error("Failed to parse metadata");
    }
  }

  parseData(metadata, data, isRetry = false) {
    try {
      const isReady = this.isSessionMetadataReady(metadata);
      const sessionKey = this.getSessionKey(metadata);

      if (isReady) {
        const sessionConfiguration = this.getSessionConfiguration(metadata);
        const mapping = sessionConfiguration[PID_MAPPING];
        const defaultUnit = sessionConfiguration[PID_DEFAULT_UNIT];

        const mappingKeys = Object.keys(mapping);

        const dataKeys = Object.keys(data);
        const items = [];

        const geohashItem = {
          ...metadata,
          [TORQUE_METRIC_LABEL_DEVICE]: this.config.devices[metadata.email],
          [METRIC_VALUE_KEY]: 6,
        }

        dataKeys.filter(k => !TORQUE_IGNORE_PIDS.includes(k))
          .forEach((k) => {
            const metricValue = parseFloat(data[k]);
            const keyWithoutPrefix = k.substring(1);
            const cleanKey =
              keyWithoutPrefix.length === 1
                ? `0${keyWithoutPrefix}`
                : keyWithoutPrefix;

            if(cleanKey === PID_GPS_LONGITUDE) {
              geohashItem[PID_GPS_LONGITUDE_NAME] = metricValue;
            } else if(cleanKey === PID_GPS_LATITUDE) {
              geohashItem[PID_GPS_LATITUDE_NAME] = metricValue;
            }

            if (mappingKeys.includes(cleanKey)) {
              const pidMapping = mapping[cleanKey];

              const item = {
                ...metadata,
                ...pidMapping,
                [TORQUE_METRIC_LABEL_DEVICE]: this.config.devices[metadata.email],
                [METRIC_VALUE_KEY]: metricValue,
              };

              if (!item[PID_UNIT]) {
                item[PID_UNIT] = defaultUnit[cleanKey];
              }

              if (Array.isArray(item[PID_UNIT])) {
                item[PID_UNIT] = item[PID_UNIT][0];
              }

              TORQUE_METRIC_LABEL_CLEAN.forEach((l) => {
                delete item[l];
              });

              items.push(item);
            } else {
              console.error(`No mapping found for '${k}', cannot report metric`);
            }
        });

        this.prometheusClient.reportMetrics(items);
        this.prometheusClient.reportLocation(geohashItem);
        
        if(this.autoResetId) {
          clearTimeout(this.autoResetId);
        }

        this.autoResetId = setTimeout(() => this.resetMetrics(metadata, items), 60 * 1000);
        
      } else {
        if (isRetry) {
          console.error(
            `Message cannot get processed due to missing metadata for session ${sessionKey}`
          );
          return;
        }

        console.warn(
          `Message cannot get processed due to missing metadata for session ${sessionKey}, re-process attempt will take place in 30 seconds`
        );

        setTimeout(() => this.parseData(metadata, data, true), 30 * 1000);
      }
    } catch (ex) {
      console.error(`Failed to parse data from request, Error: ${ex}`);
      throw Error("Failed to parse data");
    }
  }

  process(message) {
    console.debug(`Processing request, Data: ${JSON.stringify(message)}`);

    const metadata = this.extractMetadata(message);

    const data = this.extractData(message);

    const dataKeys = Object.keys(data);
    const messageType = this.getMessageType(dataKeys);
    const parser = this.messageParsers[messageType];

    if (parser) {
      parser(metadata, data);
    } else {
      throw Error("Invalid message");
    }
  }

  resetMetrics(metadata, items) {
    console.info(`Reseting metrics for session due to inactivity over the last 1 minute, Details: ${JSON.stringify(metadata)}}`);

    items.forEach(item => {
      item[METRIC_VALUE_KEY] = 0;
    });

    this.prometheusClient.process(items);
  }
}

module.exports.TorqueManager = TorqueManager;
