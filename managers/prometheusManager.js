const Prometheus = require("prom-client");
const ngeohash = require("ngeohash");

const {
  TORQUE_METRIC_GAUGE,
  TORQUE_METRIC_GEOHASH_GAUGE,
  METRIC_VALUE_KEY,
  PID_GPS_LATITUDE_NAME,
  PID_GPS_LONGITUDE_NAME,
  TORQUE_METRIC_LABEL_GEOHASH,
  TORQUE_METRIC_LABEL_EMAIL,
  TORQUE_METRIC_LABEL_TIME
} = require("../common/consts.js");

class PrometheusManager {
  constructor() {
    this.registry = new Prometheus.Registry();
    this.gauge = new Prometheus.Gauge(TORQUE_METRIC_GAUGE);
    this.gaugeGeohash = new Prometheus.Gauge(TORQUE_METRIC_GEOHASH_GAUGE);

    this.registry.registerMetric(this.gauge);
    this.registry.registerMetric(this.gaugeGeohash);
  }

  reportLocation(item) {
    console.debug(`Report location`);

    const lat = item[PID_GPS_LATITUDE_NAME];
    const lon = item[PID_GPS_LONGITUDE_NAME];

    delete item[PID_GPS_LATITUDE_NAME];
    delete item[PID_GPS_LONGITUDE_NAME];
    delete item[METRIC_VALUE_KEY];
    delete item[TORQUE_METRIC_LABEL_EMAIL];
    delete item[TORQUE_METRIC_LABEL_TIME];

    item[TORQUE_METRIC_LABEL_GEOHASH] = ngeohash.encode(lat, lon);

    this.gaugeGeohash.labels(item).set(1);
  }

  reportMetrics(items) {
    console.debug(`Report ${items.length} metrics`);

    items.forEach((item) => {
      const value = item.value;
      delete item[METRIC_VALUE_KEY];

      this.gauge.labels(item).set(value);
    });
  }

  async getMetrics() {
    return await this.registry.metrics();
  }

  getMetricsContentType() {
    return this.registry.contentType;
  }
}

module.exports.PrometheusManager = PrometheusManager;
