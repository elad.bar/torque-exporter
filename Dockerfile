FROM node:20-slim

WORKDIR /usr/src/app

COPY . .

RUN apt update && \
    apt upgrade && \
    apt install curl -y && \
    npm install

EXPOSE 8128

HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost:8128/ping

CMD [ "node", "--experimental-modules", "server.js" ]
